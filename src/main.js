import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
Vue.config.productionTip = false

// Event buss
const buss = new Vue();
window.buss = buss;

// Vuetity
import Vuetify from 'vuetify'
Vue.use(Vuetify)
import 'vuetify/dist/vuetify.min.css'

// Meta information
import Meta from 'vue-meta'
Vue.use(Meta);

// Firebase 
let app;
import firebase from './services/firebase'

// Main Vue instance
firebase.auth().onAuthStateChanged(function (user) {
  if(user && store.getters['auth/isLoggedIn'] === false) {
    store.commit('auth/storeUser', user);
    store.dispatch('auth/getSettings')
  }
  if(!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  } 
});