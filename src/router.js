import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/Home.vue'
import store from './store'
import firebase from 'firebase/app'
import 'firebase/auth'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/loop/:id',
      name: 'LoopDetails',
      //component: () => import(/* webpackChunkName: "LoopDetails" */ './pages/LoopDetails.vue')
      component: () => import('./pages/LoopDetails.vue'),
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/my-loops',
      name: 'myLoops',
      //component: () => import(/* webpackChunkName: "myLoops" */ './pages/MyLoops.vue')
      component: () => import('./pages/MyLoops.vue'),
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/notifications',
      name: 'notifications',
      //component: () => import(/* webpackChunkName: "notifications" */ './pages/Notifications.vue')
      component: () => import('./pages/Notifications.vue'),
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/settings',
      name: 'settings',
      //component: () => import(/* webpackChunkName: "settings" */ './pages/Settings/Settings.vue')
      component: () => import('./pages/Settings/Settings.vue'),
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/settings/profile',
      name: 'settingsProfile',
      //component: () => import(/* webpackChunkName: "settings" */ './pages/Settings/Profile.vue')
      component: () => import('./pages/Settings/Profile.vue'),
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/settings/password',
      name: 'settingsPassword',
      //component: () => import(/* webpackChunkName: "settings" */ './pages/Settings/Password.vue')
      component: () => import('./pages/Settings/Password.vue'),
      meta: {
        requireAuth: true,
      }
    },
    // {
    //   path: '/add',
    //   name: 'add',
    //   //component: () => import(/* webpackChunkName: "add" */ './pages/Add.vue')
    //   component: () => import('./pages/Add.vue'),
    //   meta: {
    //     requireAuth: true,
    //   }
    // },
    {
      path: '/add',
      name: 'add',
      //component: () => import(/* webpackChunkName: "add" */ './pages/AddNew.vue')
      component: () =>
        import('./pages/AddNew.vue'),
      meta: {
        requireAuth: true,
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './pages/Login.vue'),
      meta: {
        simple: true
      }
    }, 
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "register" */ './pages/Register.vue'),
      meta: {
        simple: true
      }
    }
  ]
})

// middleware auth
router.beforeEach((to, from, next) => {
  var currentUser = firebase.auth().currentUser
  var requireAuth = to.matched.some(record => record.meta.requireAuth)

  if(requireAuth && !currentUser) next('/login')
  else if (!requireAuth && currentUser) next('/')
  else next()
})

// middleware layout 
router.beforeEach((to, from, next) => {
  var currentLayout = store.getters['helper/activeLayout']
  var nextIsSimple = to.matched.some(record => record.meta.simple)

  if(currentLayout != 'simple-layout' && nextIsSimple == true) {
    store.commit('helper/setLayout', 'simple-layout')
  } else if (currentLayout != 'main-layout' && nextIsSimple == false) {
    store.commit('helper/setLayout', 'main-layout')
  }
  next()
})

export default router
