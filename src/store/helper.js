export default {
    namespaced: true,
    state: {
        loading: false,
        layout: 'main-layout',
        search: false
    },
    getters: {
        isLoading(state) {
            return state.loading
        },
        activeLayout(state) {
            return state.layout
        },
        isSearching(state) {
            return state.search
        }
    },
    mutations: {
        /* Layout mutations */
        setLayout(state, new_layout) {
            state.layout = new_layout
        },
        /* Loading mutations */
        startLoading(state) {
            state.loading = true
        },
        stopLoading(state) {
            state.loading = false
        },
        startSearch(state) {
            state.search = true
        },
        stopSearch(state) {
            state.search = false
        }
    },
    actions: {
        toggleSearch({commit, state}) {
            if(state.search == true) {
                commit('stopSearch')
            } else {
                commit('startSearch')
            }
        }
    }
}