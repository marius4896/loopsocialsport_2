import firebase from '@/services/firebase'
var db = firebase.database()
var loopRef = db.ref('loops');

export default {
    namespaced: true,
    state: {
        items: [],
        details: null,
        error: false,
    },
    getters: {
        myLoops(state, getters, rootState, rootGetters) {
            const myID = rootGetters['auth/id']
            return state.items.filter(item => item.user == myID)
        }
    },
    mutations: {
        removeLoops(state) {
            state.items = []
        },
        storeManyLoops(state, loops) {    
            Object.keys(loops).map((key) => {
                let final = {
                    ...loops[key],
                    id: key
                }
                state.items.push(final)
            })
        },      
        setDetails(state, newDetails) {
            if(typeof newDetails == 'object'){
                state.details = newDetails
            }
        },
        removeDetails(state) {
            state.details = null
        },
        setError(state) {
            state.error = true;
        },
        removeError(state) {
            state.error = false;
        }
    },
    actions: {
        async getAllLoops({commit}) {
            return await loopRef.orderByChild('createdAt').on('value', (snapshot) => {
                commit('removeLoops')
                commit('storeManyLoops', snapshot.val())
            })
        },
        async getLoopDetails({commit}, key){
            commit('removeError');
            return await loopRef.child(key).once('value', (snapshot) => {
                if(snapshot.val() == null) {
                    commit('setError')
                } else {
                    commit('setDetails', snapshot.val())
                }
               
            })
        },
        async joinLoop({commit}, payload) {
            const aq = firebase.database().ref('actionQueue');
            const key = aq.push({}).key;
            return aq.child(key).set({
                type: 'joinLoop',
                payload: payload
            })
        },
        clearLoopDetails({commit}){
            commit('removeDetails')
        }
    }
}