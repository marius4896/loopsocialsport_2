import firebase from '@/services/firebase'
var db = firebase.database()

export default {
    namespaced: true,
    state: {
         // sport settings
        sports: [{
            id: 0,
            internal: 'run',
            name: 'Running',
            icon: 'directions_run',
            component: 'running',
            type: 'free',
            default: true
        },
        {
            id: 1,
            internal: 'bike',
            name: 'Cycling',
            icon: 'directions_bike',
            component: 'cycling',
            type: 'free',
            default: false
        }],
        currentSport: 'run',
        loadedComponent: 'running',
        // stepper settings 
        currentStep: 1,
        totalSteps: 3,
        activeForm: false,
        // to be saved 
        formData: {
            type: 'run',
            author: null,
            loopTime: null,
            loopDate: null,
            location: {
                lat: 0,
                lng: 0,
                city: null,
                state: null,
                country: null,
                street: null
            },
            details: null,
            loopMeta: {},
            createdAt: new Date().toISOString(),
            views: 0
        }
    },
    getters: {
        isActive(state) {
            return state.activeForm
        }
    },
    mutations: {
        nextStep(state) {
            if(state.currentStep < state.totalSteps) {
                state.currentStep = state.currentStep + 1
            }
        },
        prevStep(state) {
            if(state.currentStep > 1 ) {
                state.currentStep = state.currentStep - 1
            }
        },
        resetStep(state) {
            state.currentStep = 1
        },
        setLoadedComponent(state, newComponent) {
            state.loadedComponent = newComponent
        },
        setActiveSport(state, newSport) {
            state.currentSport = newSport
            state.formData.type = newSport

            let component = state.sports.filter(sport => sport.internal == newSport )
            state.loadedComponent = component[0].component
        },
        setLocation(state, locationObject) {
            state.formData.location = locationObject
        },
        setState(state, stateObject) {
            state.formData[stateObject.type] = stateObject.value
        },
        resetForm(state) {
            state.formData = {
                type: 'run',
                loopTime: null,
                loopDate: null,
                location: {
                    lat: 0,
                    lng: 0,
                    city: null,
                    state: null,
                    country: null,
                    street: null
                },
                details: null,
                loopMeta: {},
                createdAt: new Date().toISOString(),
                updatedAt: null,
                views: 0,
                access: 'all', // to do private
                peopleJoining: {},
                peopleMaybe: {}
            }
        }
    },
    actions: {
        nextStep({commit}) {
            commit('nextStep')
        },
        prevStep({commit}) {
            commit('prevStep')
        },
        resetStep({commit}){
            commit('resetStep')
        },
        setLoadedComponent({commit}, newComponent) {
            commit('setLoadedComponent', newComponent)
        },
        setActiveSport({commit}, newSport) {
            commit('setActiveSport', newSport)
        },
        setLocation({commit}, locationObject) {
            commit('setLocation', locationObject)
        },
        setState({commit}, stateObject){
            commit('setState', stateObject)
        },
        async pushToStore({commit, rootGetters, state}) {
            commit('setState', {
                type: 'author',
                value: rootGetters['auth/id']
            })
            const key = await db.ref('actionQueue').push({}).key;
            try {
                let user = await firebase.auth().currentUser;
                let author = {
                    id: user.uid,
                    name: user.displayName,
                    email: user.email
                }
                const loop = {
                    ...state.formData,
                    author: author
                }
                var update = {}
                update['/loops/' + key] = loop
                update['/actionQueue/' + key] = {
                    type: 'newLoop',
                    payload: state.formData
                }
                await db.ref().update(update);
                commit('resetForm')
                 return key;
            } catch ( e) {
                return false;
            }
        }
    }
}