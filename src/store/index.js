import Vue from 'vue'
import Vuex from 'vuex'
// import modules
import auth from './auth'
import helper from './helper'
import loops from './loops'
import newloop from './newloop'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    helper,
    loops,
    newloop
  }
})
