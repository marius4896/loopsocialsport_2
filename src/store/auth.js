import firebase from 'firebase/app';
import 'firebase/database'
import 'firebase/auth';

export default {
    namespaced: true,
    state: {
        userObject: null,
        user: null,
        id: null,
        settings: {
            gender: {
                id: 2,
                name: 'Other',
            },
            favouriteSport: {
                id: 0,
                name:  "Runnig"
            }
        },
        provider: null,
        isNewUser: null,
        validationErrors: [],
    },
    getters: {
        isLoggedIn(state) {
            return state.id == null ? false : true
        },
        id(state) {
            return state.id;
        },
        loginProviders(state) {
            let providers = {}
            if(state.userObject !== null) {
                 state.userObject.providerData.forEach(profile =>  {
                    providers[profile.providerId] = {
                        provider : profile.providerId,
                        uid: profile.uid,
                        name: profile.displayName,
                        email: profile.email,
                        photo: profile.photoURL
                    }
                })
            }
            return providers;
        },
        gender(state){
            if(state.settings.length) {
                return state.settings.gender.name
            }
            return null;
        },
        email(state) {
            return state.user.email
        },
        displayName(state) {
            return state.user.displayName
        }
    },
    mutations: {
        storeUser(state, payload) {
            try {
                state.userObject = payload
                state.user = payload.user
                state.id = payload.user.uid
                state.provider = payload.additionalUserInfo.providerId
                state.isNewUser = payload.additionalUserInfo.isNewUser
            } catch(e)  {
                state.user = payload
                state.id = payload.uid
                state.provider = 'none'
                state.isNewUser = false
            }
            
        },
        emptyUser(state) {
            state.user = null
            state.id = null
            state.provider = null
            state.isNewUser = null
        },
        storeSettings(state, payload = null) {
            if(payload !== null ){
                state.settings = payload
            }
        }
    },
    actions: {
        /*
        * Register action
        * @Object  newUser - email and password needed for registration
        * @return Promise
        */
        async register({commit}, newUser) {
            commit('helper/startLoading', null, {root: true})
            try {
                let user = await firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password);
                if(user) {
                    let theUser = await firebase.auth().currentUser;
                    await theUser.updateProfile({
                        displayName: newUser.name
                    });

                    const userObject = {
                        type: 'userRegister',
                        payload: {
                            id: user.user.uid,
                            displayName: newUser.name,
                            email: newUser.email,
                            createdAt: new Date().toISOString(),
                            lastLogin: new Date().toISOString(),
                            photoURL: null,
                            isNew: true,
                            providers: [{
                                type: 'email',
                                value: newUser.email
                            }]
                        }
                    }
                    await firebase.database().ref('actionQueue/' + user.user.uid).set(userObject);
                    commit('helper/stopLoading', null, { root: true})
                    return theUser;
                }
                return false;
                
            } catch(e) {
                 commit('helper/stopLoading', null, {root: true})
                 return false;
            }
           
            // return new Promise((resolve,reject) => {
            //     firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password)
            //         .then(user => {
            //             commit('storeUser', user)
            //             new Promise((res, rej) => {
            //                 // Set display Name
            //                 let user = firebase.auth().currentUser

            //                 await user.updateProfile({
            //                     displayName: payload.displayName,
            //                     photoURL: payload.photoURL
            //                 })

            //                 // Store it in the db as well
            //                 const userObject = {
            //                     type: 'userRegister',
            //                     payload: {
            //                         id: user.user.uid,
            //                         displayName: newUser.name,
            //                         email: newUser.email,
            //                         createdAt : new Date().toISOString(),
            //                         lastLogin: new Date().toISOString(),
            //                         photoURL: null,
            //                         isNew: true,
            //                         providers: [
            //                             { type: 'email',  value: newUser.email }
            //                         ]
            //                     }
            //                 }
            //                 firebase.database().ref('actionQueue/' + user.user.uid).set(userObject)
            //                     .then(reg => {
            //                         res(user);
            //                     })
            //                     .catch(e => {
            //                         rej(e);
            //                     })
            //             });
            //             commit('helper/stopLoading', null, {root: true})
            //             resolve(user)
            //         })
            //         .catch(err => {
            //             commit('helper/stopLoading', null, {root: true})
            //             reject(err)
            //         })
            // })
        },

        /*
        * Login action
        * @Object  credentials - email and password
        * @return Promise
        */
        login({commit}, credentials) {
            commit('helper/startLoading', null, {root: true})
            
            return new Promise((resolve, reject) => {
                firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password)
                .then(function(user) {
                    commit('storeUser', user)
                    commit('helper/stopLoading', null, {root: true})
                    resolve(user)
                })
                .catch(function(err) {
                    commit('helper/stopLoading', null, {root: true})
                    reject(err)
                })
            })
                
        },
        /**
         * Change password
         * @param {state} param0 
         * @param {payload} payload 
         */
        async changePassword({state, commit}, payload) {
            commit('helper/startLoading', null, {root: true})
            let email = state.user.email
            let user = await firebase.auth().currentUser
            var credential = firebase.auth.EmailAuthProvider.credential(
                email,
                payload.current
            );

            return new Promise((resolve, reject) => {
                user.reauthenticateAndRetrieveDataWithCredential(credential)
                    .then(() => {
                        user.updatePassword(payload.newPassword)
                            .then(()  => {
                                    commit('helper/stopLoading', null, {root: true})
                                    resolve()
                            })
                            .catch((error) => {
                                commit('helper/stopLoading', null, {root: true})
                                reject(error)
                            });
                    })
                    .catch(e => {
                        commit('helper/stopLoading', null, {root: true})
                        reject(e)
                    })
            })
        },

        /**
         * Update profile
         * @param {commit} param0 
         * @param {payload} payload {displayName, photoURL}
         */
        async updateProfile({commit, dispatch}, payload) {
            commit('helper/startLoading', null, {root: true})
            let user = await firebase.auth().currentUser
            let rootRef = firebase.database().ref();

            await user.updateProfile({
                displayName: payload.displayName,
                photoURL: payload.photoURL
            })
            await rootRef.child('users/' + user.uid).update({
                gender: payload.gender,
                favouriteSport: payload.favouriteSport
            })
            await dispatch('getSettings')
            commit('helper/stopLoading', null, {root: true})
        },

        async logout({commit}) {
            commit('helper/startLoading', null, {root: true})
            
            return new Promise((resolve, reject) => {
                commit('helper/startLoading', null, {root: true})
                firebase.auth().signOut()
                    .then(response => {
                        commit('emptyUser')
                        commit('helper/stopLoading', null, {root: true})
                        resolve(response)
                    }).catch(err => {
                        commit('helper/stopLoading', null, {root: true})
                        reject(err)
                    })
            })
        },

        async getSettings({state, commit}) {
            return await firebase.database().ref('/users/' + state.id).once('value', (snap) => {
                return  commit('storeSettings', Object.assign({}, snap.val()))
            })
        }
    }
}