import {mapGetters, mapState} from 'vuex'
export default {
    computed: {
        ...mapState('auth', {
            user: state => state.user
        }),
        ...mapGetters('auth', {
            isLoggedIn : 'isLoggedIn',
            id: 'id'
        })
    }
}