export default {
    methods: {
        geolocate() {
            navigator.geolocation.getCurrentPosition(position => {
                this.place = position
                this.center = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

            });
        },
        extractLocation(place) {
            var city, state, country, street, lat, lng;
            
            var address = place.address_components;
            address.forEach(function(component) {
                var types = component.types;
                if (types.indexOf('locality') > -1) {
                    city = component.long_name;
                }

                if (types.indexOf('administrative_area_level_1') > -1) {
                    state = component.short_name;
                }

                if (types.indexOf('country') > -1) {
                    country = component.long_name;
                }

                if (types.indexOf('route') > -1) {
                    street = component.long_name;
                }
            });
            lat =  place.geometry.location.lat()
            lng = place.geometry.location.lng()

            return {
                lat: lat ? lat : null,
                lng: lng ? lng : null,
                city: city ? city : null,
                state: state ? state: null,
                country: country ? country : null,
                street: street ? street : null
            }
        }
    }
}