import firebaseApp from 'firebase/app';
import 'firebase/auth';
import 'firebase/database'

 const config = {
    apiKey: "AIzaSyDjqNnGSK80nRAdg2CqrLb5hnh08VPbvwI",
    authDomain: "loopsocialsport-com.firebaseapp.com",
    databaseURL: "https://loopsocialsport-com.firebaseio.com",
    projectId: "loopsocialsport-com",
    storageBucket: "loopsocialsport-com.appspot.com",
    messagingSenderId: "302006612434"
};
// initialize app
var firebase = firebaseApp.initializeApp(config)

export default firebase;


